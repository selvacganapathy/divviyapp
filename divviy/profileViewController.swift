//
//  profileViewController.swift
//  divviy
//
//  Created by PSA Developer on 10/08/2016.
//  Copyright © 2016 PSA Developer. All rights reserved.
//

import UIKit
import Parse

class profileViewController: UIViewController {

    @IBOutlet weak var profileImage: UIImageView!
  
    override func viewDidLoad()
    {
        super.viewDidLoad()
        profileImage.layer.borderWidth = 1
        profileImage.layer.masksToBounds = false
        profileImage.layer.borderColor = UIColor.blackColor().CGColor
        profileImage.layer.cornerRadius = profileImage.frame.height/2
        profileImage.layer.cornerRadius = profileImage.frame.width/2
        profileImage.clipsToBounds = true
        // Do any additional setup after loading the view.
        let currentUser = PFUser.currentUser()
        if currentUser != nil {
            print(PFUser.currentUser())
            // Do stuff with the user
        } else {
            // Show the signup or login screen
        }
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
